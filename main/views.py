from django.shortcuts import get_object_or_404, render
from django.views.generic import TemplateView, ListView, DetailView,CreateView, DeleteView, UpdateView

from main.models import Student, University, Lesson
from main.forms import StudentForm
from django.urls import reverse


class IndexView(TemplateView):
    template_name = "index.html"


class StudentsListView(ListView):
    model = Student
    context_object_name = 'students'
    template_name = "students/students.html"


class StudentAddView(CreateView):
    template_name = 'students/add_student.html'
    success_url = 'students'
    form_class = StudentForm
    queryset = Student.objects.all()


class StudentUpdateView(UpdateView):
    template_name = 'students/add_student.html'
    form_class = StudentForm

    def get_object(self):
        return get_object_or_404(Student, id=self.kwargs.get("pk"))

    def get_success_url(self):
        return reverse('students')


class StudentDeleteView(DeleteView):

    def get_object(self):
        return get_object_or_404(Student, id=self.kwargs.get("pk"))

    def get_success_url(self):
        return reverse('students')


class StudentView(DetailView):
    template_name = "students/student.html"

    def get_object(self):
        return get_object_or_404(Student, id=self.kwargs.get("pk"))


class UniversitiesListView(ListView):
    queryset = University.objects.prefetch_related("lesson_university__lesson")
    context_object_name = 'universities'
    template_name = "universities.html"


class LessonsListView(ListView):
    queryset = Lesson.objects.prefetch_related("university_lesson__university")
    context_object_name = 'lessons'
    template_name = "lessons.html"


def students_app(request):
    return render(request, 'vue_app.html')
