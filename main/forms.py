from django.forms import ModelForm
from main.models import Student
from django import forms
from django.core.exceptions import ValidationError


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = "__all__"

    def clean_name(self):
        name = self.cleaned_data['name']
        if len(name) < 6:
            raise ValidationError('Name is short')
        name = name + '1'
        return name

    def clean(self):
        cleaned_data = super(StudentForm, self).clean()
        name = cleaned_data.get("name")
        birthday = cleaned_data.get("birthday")

        if name and birthday:
            if " " not in name:
                raise forms.ValidationError("The name must have a ' ', please enter a valid name")

        return cleaned_data
