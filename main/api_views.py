from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet

from main.models import Student
from main.serializers import StudentsSerializer


class StudentsView(ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentsSerializer

    # def get_object(self):
    #     return get_object_or_404(Student)
