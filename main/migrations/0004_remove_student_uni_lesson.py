# Generated by Django 2.2.12 on 2020-12-02 11:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20201202_1043'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='uni_lesson',
        ),
    ]
