from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('students', views.StudentsListView.as_view(), name='students'),
    path('universities', views.UniversitiesListView.as_view(), name='universities'),
    path('lessons', views.LessonsListView.as_view(), name='lessons'),
    path('student/<int:pk>/', views.StudentView.as_view(), name='student_view'),
    path('student_delete/<int:pk>/', views.StudentDeleteView.as_view(), name='student_delete'),
    path('new_student', views.StudentAddView.as_view(), name='add_new_student'),
    path('student_update/<int:pk>/', views.StudentUpdateView.as_view(), name='student_update'),
    path('students_app', views.students_app)
]