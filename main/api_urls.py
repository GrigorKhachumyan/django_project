from rest_framework.routers import SimpleRouter
from main.api_views import StudentsView

router = SimpleRouter()

router.register('api/students', StudentsView)
