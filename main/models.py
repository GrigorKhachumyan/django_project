from django.db import models


class Address(models.Model):
    name = models.CharField(unique=True, max_length=45)

    class Meta:
        managed = True
        db_table = 'addresses'


class Lesson(models.Model):
    name = models.CharField(unique=True, max_length=45)

    class Meta:
        managed = True
        db_table = 'lessons'


class Student(models.Model):
    name = models.CharField(max_length=200)
    birthday = models.DateField(blank=True, null=True)
    address = models.ForeignKey('Address', models.CASCADE, related_name="student_address", null=True)
    # uni_lessons = models.ManyToManyField('UniversityLesson', models.CASCADE, related_name="student_uni_lesson", null=True)

    class Meta:
        managed = True
        db_table = 'students'

    def first_name(self):
        return self.name.split(" ")[0]


class University(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'universities'


class UniversityLesson(models.Model):
    university = models.ForeignKey('University', models.CASCADE, related_name='lesson_university')
    lesson = models.ForeignKey('Lesson', models.CASCADE, related_name='university_lesson')

    class Meta:
        managed = True
        db_table = 'university_lesson'
