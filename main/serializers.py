from rest_framework.serializers import ModelSerializer

from main.models import Student


class StudentsSerializer(ModelSerializer):
    class Meta:
        model = Student
        fields = ['name', 'birthday']