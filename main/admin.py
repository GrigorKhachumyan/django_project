from django.contrib import admin
from main.models import Address, Lesson, Student, University, UniversityLesson


admin.site.register(Address)
admin.site.register(Lesson)
# admin.site.register(StudentAddress)
# admin.site.register(StudentLesson)
# admin.site.register(StudentUniversity)
admin.site.register(Student)
admin.site.register(University)
admin.site.register(UniversityLesson)
